﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Autor: Robson Calixto
/// Data: 26/03/2016
/// Validação de xml criado em tempo de execução com XSD
/// </summary>
namespace ValidacaoXml
{
    class Program
    {
        static void Main(string[] args)
        {

           #region Criação dos Arquivos Xml e XSD
            try
            {

                string xml = @"<?xml version=""1.0"" encoding=""utf-8""?> 
                            <principal>
                                <pessoa>
                                    <nome>Robson Calixto</nome>
                                    <nome2>55587 - teste numero</nome2>
                                    <nome3></nome3>
                                    <idade>22</idade>
                                    <idade2>Teste caracter</idade2>
                                    <idade3>99.9</idade3>
                                    <nascimento>24/10/1993</nascimento>
                                    <nascimento2></nascimento2>
                                    <nascimento3>teste caracter</nascimento3>
                                </pessoa>
                            </principal>";




                string xsd = @"<xs:schema attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
                            <xs:simpleType name=""emptyString"">
                                <xs:restriction base=""xs:string"">
                                <xs:enumeration value=""""/>
                                </xs:restriction>
                            </xs:simpleType>
                            <xs:simpleType name=""datatype"">
                                    <xs:restriction base=""xs:string"">
                                    <xs:pattern value=""\d{2}[/]\d{2}[/]\d{4}""></xs:pattern>
                                    <xs:length value=""10""/>
                                </xs:restriction>
                            </xs:simpleType>
                            <xs:simpleType name=""stringtype"">
                                <xs:restriction base=""xs:string"">
                                    <xs:pattern value=""^[a-zA-Z \t\n\r\f\v]{1,120}$""></xs:pattern>
                                </xs:restriction>
                            </xs:simpleType>
                            <xs:simpleType name=""inttype"">
                                <xs:restriction base=""xs:positiveInteger""/>
                            </xs:simpleType>
                            <xs:complexType name=""pessoatype"">
                                <xs:sequence>
                                    <xs:element name=""nome"" type=""stringtype""/>  
                                    <xs:element name=""nome2"" type=""stringtype""/>  
                                    <xs:element name=""nome3"" type=""stringtype""/>  
                                    <xs:element name=""idade"" nillable=""true"">
                                        <xs:simpleType>
                                            <xs:union memberTypes=""inttype emptyString""/>
                                        </xs:simpleType>
                                    </xs:element>
                                    <xs:element name=""idade2"" nillable=""true"">
                                        <xs:simpleType>
                                            <xs:union memberTypes=""inttype emptyString""/>
                                        </xs:simpleType>
                                    </xs:element>
                                    <xs:element name=""idade3"" nillable=""true"">
                                        <xs:simpleType>
                                            <xs:union memberTypes=""inttype emptyString""/>
                                        </xs:simpleType>
                                    </xs:element>           
                                    <xs:element name=""nascimento"" nillable=""true"">
                                        <xs:simpleType>
                                            <xs:union memberTypes=""datatype emptyString""/>
                                        </xs:simpleType>
                                    </xs:element>           
                                    <xs:element name=""nascimento2"" nillable=""true"">
                                        <xs:simpleType>
                                            <xs:union memberTypes=""datatype emptyString""/>
                                        </xs:simpleType>
                                    </xs:element>           
                                    <xs:element name=""nascimento3"" nillable=""true"">
                                        <xs:simpleType>
                                            <xs:union memberTypes=""datatype emptyString""/>
                                        </xs:simpleType>
                                    </xs:element>
                                </xs:sequence>
                            </xs:complexType>
                            <xs:complexType name=""principaltype"">
                                <xs:sequence>
                                <xs:element name=""pessoa"" type=""pessoatype""/>
                                </xs:sequence>  
                            </xs:complexType>

                            <xs:element name=""principal"" type=""principaltype""/>   
                        </xs:schema>";
                #endregion
                ///<summary>
                ///Instância da classe que realiza a validação do XML sobre o arquivo XSD
                /// </summary>
                /// 
                var ValidaDocumentoXml = new Validacao();

                ///<summary>
                ///Instância da lista para tratar os erros de validação
                ///</summary>
                List<Exception> RetornoErros = ValidaDocumentoXml.valida(xml, xsd);


                #region Tratamento e exibição das mensagens de erro

                if (RetornoErros.Count > 0)
                {
                    foreach (var Erros in RetornoErros)
                    {

                        Console.WriteLine("{0}\r\n", Erros.Message);
                    }

                }
                else
                {
                    Console.WriteLine("Parabens o seu Arquivo tah Padrao !!!");
                }

                #endregion


                Console.ReadKey();
                #region Tratameto de erro principal
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }

            #endregion
        }
    }
}
