﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace ValidacaoXml
{
    /// <summary>
    /// Classe responsável por validar arquivo xml com um padrão XSD
    /// </summary>
    public class Validacao
    {
        /// <summary>
        /// Construtor padrão vazio
        /// </summary>
        public Validacao()
        {

        }

        /// <summary>
        /// Metodo que executa a validação do XML
        /// </summary>
        /// <param name="textXml">string de xml sem tratamento</param>
        /// <param name="textSchema">arquivo xsd em formato de string </param>
        /// <returns>o metódo retorna os erros capturados durante a validação do arquivo</returns>
        public List<Exception> valida(string textXml, string textSchema)
        {
            try
            {


                //Recuperar arquivo XSD do diretório Raiz do projeto.
                /*-------------------------------------------------------------------*/
                /*     var file = new FileInfo(Path.Combine(Path.GetDirectoryName(   */   
                /*     Assembly.GetExecutingAssembly().Location), @"SchemaPd.xsd")); */  
                /*                                                                   */           
                /*-------------------------------------------------------------------*/



                ///<summary>
                ///instância da classe que cria um espaço de memória para o arquivo XSD
                ///</summary>
                XmlSchemaSet schemas = new XmlSchemaSet();
                
                
                ///<param name="textSchema">arquivo que determina o padrão a ser validado</param>
                ///<remarks>A string vazia representa o nó que será validado. Quando Vazio validará todo o arquivo</remarks>
                schemas.Add("", XmlReader.Create(new StringReader(textSchema)));

                ///<summary>
                ///Convertendo a string em arquivo xml e armazendo na memória
                ///</summary>
                XDocument docTextXml = XDocument.Parse(textXml);

                ///<summary>
                ///Lista de erro dos campos que não foram validados
                /// </summary>
                List<Exception> lErros = new List<Exception>();

                
                docTextXml.Validate(schemas, (o, e) => { lErros.Add(e.Exception); });

                return lErros;
            }
            catch (Exception err)
            {
                throw err;
            }
        }
    }
}
